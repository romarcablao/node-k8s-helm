const express = require("express");
const router = require("./src");

const app = express();
const port = 80;

app.get("/", (req, res) => res.send("v1.25.0: Welcome to CloudSpark!\n"));
app.get("/version", (req, res) => res.send("v1.25.0\n"));
app.use("/", router);

app.listen(port, () => console.log(`App listening on port ${port}!`));
