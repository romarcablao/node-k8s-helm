const express = require("express");
const { welcomeMessage } = require("../operations/welcome");

const router = express.Router();

router.get("/", async (req, res) => {
  res.json({ message: "This is a valid api.", valid: true });
});

router.get("/welcome", async (req, res) => res.send(welcomeMessage()));

module.exports = router;
