# Simple Node App Deployment on EKS Cluster Using Helm

[![DockerHub](https://img.shields.io/badge/DockerHub-romarcablao-blue)](https://hub.docker.com/r/romarcablao)
[![GitHub](https://img.shields.io/badge/GitHub-romarcablao-lightgrey)](https://github.com/romarcablao)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-romarcablao-blue)](https://linkedin.com/in/romarcablao)

## Dependencies

1. [NodeJS](https://nodejs.org/en/download/)
2. [Docker](https://docs.docker.com/install/)
3. [Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

## Instructions

1. Pull or clone the repository

   ```bash
   git clone https://gitlab.com/romarcablao/node-k8s-helm.git
   cd node-k8s-helm
   ```

2. Simply push to this repository and check the updates here: 
 
    Staging: http://staging.thecloudspark.com  |  Prod: http://thecloudspark.com

## Setup and Deployment Flow

1. Create an [AWS EKS Cluster](https://aws.amazon.com/eks/).
2. Add existing kubernetes cluster on your GitLab project (or other cicd platform).
3. Install GitLab-managed apps (helm tiller and runner).
4. Set-up gitlab-ci.yaml. You can use some of the docker images I have created here [Docker Hub](https://hub.docker.com/r/romarcablao).
5. Set-up GitLab variables and push your created app.