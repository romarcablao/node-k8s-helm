#!/bin/bash

#Get current helm revision (if any)
LAST_REV=$(helm -n node-k8s-helm-prod list | awk '{print $3}' | sed -n 2p)

#Case if theres existing revision or not
if [[ -z "$LAST_REV" ]]; then
    echo "Cannot perform roll-back. Currently at the initial deployment"
else
    ROLLBACK_REV=$(($LAST_REV - 1))
    echo "Current revision: $LAST_REV" && echo "Target Revision for Rollback: $ROLLBACK_REV"
    helm -n ${KUBE_NAMESPACE} rollback ${HELM_CHART_NAME} ${ROLLBACK_REV}
    echo "Rolled back to version $ROLLBACK_REV"
fi